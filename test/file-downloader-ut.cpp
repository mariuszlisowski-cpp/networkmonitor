#include <network-monitor/file-downloader.hpp>

#include <boost/asio.hpp>
#include <boost/test/unit_test.hpp>

#include <filesystem>
#include <fstream>
#include <memory>
#include <string>

namespace utf = boost::unit_test;

BOOST_AUTO_TEST_SUITE(network_monitor)

struct Fixture
{
    Fixture() : downloader(std::make_unique<nm::Downloader>())
    {
        file_url = "https://ltnm.learncppthroughprojects.com/network-layout.json";
        file_location = std::filesystem::path(TEST_NETWORK_LAYOUT);
    }

    std::unique_ptr<nm::Downloader> downloader;
    std::string file_url;
    std::filesystem::path file_location;
};

BOOST_AUTO_TEST_CASE(file_downloader, *utf::disabled())
{
    const std::string fileUrl {"https://ltnm.learncppthroughprojects.com/network-layout.json"};
    const auto destination {std::filesystem::temp_directory_path() / "network-layout.json"};

    /* download the file */
    nm::Downloader downloader;
    bool downloaded {downloader.downloadFile(fileUrl, destination, TEST_CACERT_PEM)};
    BOOST_CHECK(downloaded);
    BOOST_CHECK(std::filesystem::exists(destination));

    /* check the content of the file - we cannot check the whole file content as it changes over time,
       but we can at least check some expected file properties */
    {
        const std::string expectedString {"\"stations\": ["};
        std::ifstream file {destination};
        std::string line {};
        bool foundExpectedString {false};
        while (std::getline(file, line)) {
            if (line.find(expectedString) != std::string::npos) {
                foundExpectedString = true;
                break;
            }
        }
        BOOST_CHECK(foundExpectedString);
    }

    /* clean up */
    std::filesystem::remove(destination);
}

BOOST_FIXTURE_TEST_CASE(parse_json_file, Fixture)
{
    std::optional<nlohmann::json> parsed;
    if (!std::filesystem::exists(file_location))
    {
        std::cerr << "WRN: couldn't find JSON file: downloading..." << std::endl;
        BOOST_CHECK(downloader->downloadFile(file_url, file_location));
    }
    std::cout << "INF: JSON file found in the path! Parsing..." << std::endl;

    parsed = downloader->parseJsonFile(file_location);
    BOOST_CHECK(parsed.has_value());

    BOOST_CHECK(parsed->is_object());
    BOOST_CHECK(parsed->contains("lines"));
    BOOST_CHECK(parsed->at("lines").size() > 0);
    BOOST_CHECK(parsed->contains("stations"));
    BOOST_CHECK(parsed->at("stations").size() > 0);
    BOOST_CHECK(parsed->contains("travel_times"));
    BOOST_CHECK(parsed->at("travel_times").size() > 0);
    
    std::cout << "INF: JSON file parsed successfully!" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END()
