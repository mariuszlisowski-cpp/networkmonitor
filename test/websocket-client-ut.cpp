#include <network-monitor/websocket-client.hpp>
#include <network-monitor/stomp-client.hpp>

#include <boost/test/unit_test.hpp>
#include <boost/asio/ssl/context.hpp>

#include <filesystem>
#include <iostream>
#include <string>

using nm::WebSocketClient;

BOOST_AUTO_TEST_SUITE(network_monitor)

bool checkResponse(const std::string& response)
{
    /* we do not parse the whole message - we only check that it contains some expected items */
    bool ok {true};
    ok &= response.find("ERROR") != std::string::npos;
    ok &= response.find("ValidationInvalidAuth") != std::string::npos;
    return ok;
}

BOOST_AUTO_TEST_CASE(cacert_pem)
{
    BOOST_CHECK(std::filesystem::exists(TEST_CACERT_PEM));
}

BOOST_AUTO_TEST_CASE(prepare_stomp_login_frame)
{
    const std::string host {"transportforlondon.com"};
    const std::string login {"fake_username"};
    const std::string password {"fake_password"};
    
    stomp::StompClient stomp_client;
    auto stomp_frame {stomp_client.prepareLoginFrame("STOMP", "1.2", host, login, password)};

    auto stomp_frame_str {stomp_client.convertFrameToString(stomp_frame)};
    std::cout << stomp_frame_str << std::endl;

    std::stringstream ss {};
    ss << "STOMP" << std::endl
       << "accept-version:1.2" << std::endl
       << "host:transportforlondon.com" << std::endl
       << "login:" << login << std::endl
       << "passcode:" << password << std::endl
       << std::endl
       << '\0';

    BOOST_CHECK_EQUAL(stomp_frame_str, ss.str());
}

BOOST_AUTO_TEST_CASE(send_stomp_login_frame)
{
    const std::string url {"ltnm.learncppthroughprojects.com"};
    const std::string endpoint {"/network-events"};
    const std::string port {"443"};

    const std::string host {"transportforlondon.com"};
    const std::string login {"fake_username"};
    const std::string password {"fake_password"};

    std::string response;

    stomp::StompClient stomp_client;
    auto stomp_frame {stomp_client.prepareLoginFrame("STOMP", "1.2", host, login, password)};
    const std::string message {stomp_client.convertFrameToString(stomp_frame)};

    boost::asio::io_context io_context {};
    boost::asio::ssl::context tls_context {boost::asio::ssl::context::tlsv12_client};
    WebSocketClient client {url, endpoint, port, io_context, tls_context};

    /* load a certification authority file for performing verification */
    tls_context.load_verify_file(TEST_CACERT_PEM);

    /* flags to check that the connection, send, receive functions work as expected */
    bool connected {false};
    bool messageSent {false};
    bool messageReceived {false};
    bool disconnected {false};

    /* callbacks */
    auto onSend_user {[&messageSent](auto ec) {
        messageSent = !ec;
    }};

    auto onConnect_user {[&client, &connected, &onSend_user, &message](auto ec) {
        connected = !ec;
        if (!ec) {
            client.send(message, onSend_user);
        }
    }};

    auto onClose_user {[&disconnected](auto ec) {
        disconnected = !ec;
    }};

    auto onReceive_user {[&client,
                          &onClose_user,
                          &messageReceived,
                          &message,
                          &response](auto ec, auto received) {
        messageReceived = !ec;
        response = received;
        client.close(onClose_user);
    }};

    /* call io_context::run for asynchronous callbacks to run */
    client.connect(onConnect_user, onReceive_user);
    io_context.run();

    BOOST_CHECK(connected);
    BOOST_CHECK(messageSent);
    BOOST_CHECK(messageReceived);
    BOOST_CHECK(disconnected);
    BOOST_CHECK(checkResponse(response));
}

BOOST_AUTO_TEST_CASE(WebSocketClient_echo)
{
    const std::string url {"ltnm.learncppthroughprojects.com"};
    const std::string endpoint {"/echo"};
    const std::string port {"443"};
    const std::string message {"Hello Echo!"};

    boost::asio::io_context io_context {};
    boost::asio::ssl::context tls_context {boost::asio::ssl::context::tlsv12_client};
    WebSocketClient client {url, endpoint, port, io_context, tls_context};

    /* load a certification authority file for performing verification */
    tls_context.load_verify_file(TEST_CACERT_PEM);

    /* flags to check that the connection, send, receive functions work as expected */
    bool connected {false};
    bool messageSent {false};
    bool messageReceived {false};
    bool messageMatches {false};
    bool disconnected {false};

    /* callbacks */
    auto onSend_user {[&messageSent](auto ec) {
        messageSent = !ec;
    }};

    auto onConnect_user {[&client, &connected, &onSend_user, &message](auto ec) {
        connected = !ec;
        if (!ec) {
            client.send(message, onSend_user);
        }
    }};

    auto onClose_user {[&disconnected](auto ec) {
        disconnected = !ec;
    }};

    auto onReceive_user {[&client,
                         &onClose_user,
                         &messageReceived,
                         &messageMatches,
                         &message](auto ec, auto received) {
        messageReceived = !ec;
        messageMatches = message == received;
        client.close(onClose_user);
    }};

    /* call io_context::run for asynchronous callbacks to run */
    client.connect(onConnect_user, onReceive_user);
    io_context.run();

    /* when we get here, the io_context::run function has run out of work to do */
    BOOST_CHECK(connected);
    BOOST_CHECK(messageSent);
    BOOST_CHECK(messageReceived);
    BOOST_CHECK(disconnected);
    BOOST_CHECK(messageMatches);
}

BOOST_AUTO_TEST_SUITE_END()
