#pragma once

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>
#include <boost/beast/core/flat_buffer.hpp>
#include <boost/beast/core/tcp_stream.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/websocket/rfc6455.hpp>
#include <boost/system/error_code.hpp>

#include <iostream>
#include <string>

namespace nm {

class WebSocketClient {
public:
    WebSocketClient(const std::string& url,
                    const std::string& endpoint,
                    const std::string& port,
                    boost::asio::io_context& io_context,
                    boost::asio::ssl::context& tls_context);
    ~WebSocketClient();

    void connect(
        std::function<void(const boost::system::error_code&)> onConnect_user = nullptr,
        std::function<void(const boost::system::error_code&, std::string&&)> onMessage_user = nullptr,
        std::function<void(const boost::system::error_code&)> onDisconnect_user = nullptr);
    void send(
        const std::string& message,
        std::function<void(const boost::system::error_code&)> onSend = nullptr);
    void close(std::function<void(const boost::system::error_code&)> onClose);

private:
    void onResolve(const boost::system::error_code& ec, const boost::asio::ip::tcp::resolver::results_type& endpoint);
    void onConnect(const boost::system::error_code& ec);
    void onHandshake(const boost::system::error_code& ec);
    void onTlsHandshake(const boost::system::error_code& ec);
    void onRead(const boost::system::error_code& ec, size_t bytes_transferred);
    void listenToIncomingMessage(const boost::system::error_code& ec);

    std::string url;
    std::string endpoint;
    std::string port;
    boost::beast::flat_buffer read_buffer;
    boost::asio::ip::tcp::resolver resolver;
    /* layers: stream < tls < tcp > > */
    boost::beast::websocket::stream<boost::beast::ssl_stream<boost::beast::tcp_stream>> websocket;
    bool closed{true};

    std::function<void (boost::system::error_code)> onConnect_ {nullptr};
    std::function<void (boost::system::error_code, std::string&&)> onReceive_ {nullptr};
    std::function<void (boost::system::error_code)> onDisconnect_ {nullptr};
};

} // namespace nm
