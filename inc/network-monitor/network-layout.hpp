#pragma once

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace nm {
/* A station, line, or route ID */
using Id = std::string;

struct Station {
    Id id;
    std::string name;

    bool operator==(const Station& other) const {
        return id == other.id;
    }
};

/*  Network route
 *  Each underground line has one or more routes. A route represents a single
 *  possible journey across a set of stops in a specified direction.
 *  There may or may not be a corresponding route in the opposite direction of travel.
 *
 *  A Route struct is well formed if:
 *  - `id` is unique across all lines and their routes in the network.
 *  - the `lineId` line exists and has this route among its routes.
 *  - `stops` has at least 2 stops.
 *  - `startStationId` is the first stop in `stops`.
 *  - `endStationId` is the last stop in `stops`.
 *  - every `stationId` station in `stops` exists.
 *  - every stop in `stops` appears only once.
 */
struct Route {
    Id id;
    std::string direction;
    Id lineId;
    Id startStationId;
    Id endStationId;
    std::vector<Id> stops;

    bool operator==(const Route& other) const {
        return id == other.id;
    }
};

/*  Network line
 *  A line is a collection of routes serving multiple stations.
 *
 *  A Line struct is well formed if:
 *  - `id` is unique across all lines in the network.
 *  - `routes` has at least 1 route.
 *  - every route in `routes` is well formed.
 *  - every route in `routes` has a `lineId` that is equal to this line `id`.
 */
struct Line {
    Id id;
    std::string name;
    std::vector<Route> routes;

    bool operator==(const Line& other) const {
        return id == other.id;
    }
};

/* Network layout as a weighted and directed GRAPH structure
 *
 * Graph components:
 * - node represents a station and contains information about connected edges
 *   and passangers present at the given moment on that station
 * - edge represents a piece of the route between stations (and is part of the internal route),
 *   is directed (thus contains the next station pointer) and
 *   is weighted (thus contains travel time)
 * - internal route representation (is part of the internal line)
 * - internal line representation (contains the internal routes)
 */
struct Edge;
struct LineInternal;
struct RouteInternal;

struct Node
{
    Id stationId;
    std::string name;
    long long passengerCount {};
    std::vector<std::shared_ptr<Edge>> edges;

    /* A method to find the edge specific to a route. */
    auto findEdgeForRoute(const std::shared_ptr<RouteInternal>& route) const;
};

struct Edge
{
    std::shared_ptr<RouteInternal> route;                               // what route an edge is a part of
    std::shared_ptr<Node> nextStop;
    unsigned travelTime {};
};

struct RouteInternal
{
    Id id;
    std::shared_ptr<LineInternal> line;                                 // what line a route is a part of
    std::vector<std::shared_ptr<Node>> stops;                           // what stations a route consists of
};

struct LineInternal
{
    Id id;
    std::string name;
    std::unordered_map<Id, std::shared_ptr<RouteInternal>> routes;      // map line routes by their ID
};

} // namespace nm
