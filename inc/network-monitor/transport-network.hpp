#pragma once

#include "network-layout.hpp"
#include <unordered_map>

namespace nm {
/* Passenger event */
struct PassengerEvent {
    enum class Type {
        IN,
        OUT
    };

    Id stationId;
    Type type {Type::IN};
};

/* Underground network representation */
class TransportNetwork {
public:
    TransportNetwork() = default;                                                   // default constructor
    TransportNetwork(const TransportNetwork& copied) = delete;                      // copy constructor
    TransportNetwork(TransportNetwork&& moved) = delete;                            // move constructor
    TransportNetwork& operator=(const TransportNetwork& copied) = delete;           // copy assignment operator
    TransportNetwork& operator=(TransportNetwork&& moved) = delete;                 // move assignment operator
    ~TransportNetwork() = default;                                                  // destructor

    /* Add a station to the network.
       Returns false if there was an error while adding the station to the network.
       This function assumes that the Station object is well-formed.
       The station cannot already be in the network. */
    bool addStation(const Station& station);

    /* Add a line to the network.
       Returns false if there was an error while adding the station to the network.
       This function assumes that the Line object is well-formed.
       All stations served by this line must already be in the network.
       The line cannot already be in the network. */
    bool addLine(const Line& line);

    /* Record a passenger event at a station.
       Returns false if the station is not in the network or if the passenger event is not reconized. */
    bool recordPassengerEvent(const PassengerEvent& event);

    /* Get the number of passengers currently recorded at a station.
       The returned number can be negative: This happens if we start recording
       in the middle of the day and we record more exiting than entering passengers.
       Throws std::runtime_error if the station is not in the network. */
    long long getPassengerCount(const Id& station) const;

    /* Get list of routes serving a given station.
       Returns an empty vector if there was an error or if the station has legitimately no routes serving it.
       The station must already be in the network. */
    std::vector<Id> getRoutesServingStation(const Id& station) const;

    /* Set the travel time between 2 adjacent stations.
       Returns false if there was an error while setting the travel time between the two stations.
       The travel time is the same for all routes connecting the two stations directly.
       The two stations must be adjacent in at least one line route.
       The two stations must already be in the network.*/
    bool setTravelTime(const Id& stationA, const Id& stationB, const unsigned int travelTime);

    /* Get the travel time between 2 adjacent stations.
       Returns 0 if the function could not find the travel time between the two stations
       or if station A and B are the same station.
       The travel time is the same for all routes connecting the two stations directly.
       The two stations must be adjacent in at least one line route.
       The two stations must already be in the network. */
    unsigned int getTravelTime(const Id& stationA, const Id& stationB) const;

    /* Get the total travel time between any 2 stations, on a specific route.
       The total travel time is the cumulative sum of the travel times
       between all stations between `stationA` and `stationB`.
       Returns 0 if the function could not find the travel time between the
       two stations, or if station A and B are the same station.
       The two stations must be both served by the `route`. The two stations must already be in the network. */
    unsigned int getTravelTime(const Id& line, const Id& route, const Id& stationA, const Id& stationB) const;

private:
    /* Map station and lines by ID.
       Our graph does not have a root node, and we will need to access specific stations (nodes) on demand.
       For this reason, we maintain maps to access stations, lines and routes by ID.
       We do not map line routes here, as they are mapped within each line representation (LineInternal struct). */
    std::unordered_map<Id, std::shared_ptr<Node>> stations;
    std::unordered_map<Id, std::shared_ptr<LineInternal>> lines;

    std::shared_ptr<Node> getStation(const Id& id) const;
    std::shared_ptr<LineInternal> getLine(const Id& line) const;
    bool addRouteToLine(const Route& route, const std::shared_ptr<LineInternal>& lineInternal);
};

} // namespace nm
