#pragma once

#include <string>
#include <unordered_map>

namespace stomp {
const static std::string version_header_title {"accept-version:"};
const static std::string host_header_title {"host:"};
const static std::string login_header_title {"login:"};
const static std::string password_header_title {"passcode:"};

class StompClient
{
    struct StompFrame
    {
        std::string command;
        std::unordered_map<std::string, std::string> headers;
        std::string body;
    };
public:
    StompFrame prepareLoginFrame(const std::string& command,
                                 const std::string& version,
                                 const std::string& host,
                                 const std::string& login,
                                 const std::string& password);

    std::string convertFrameToString(const StompFrame& frame);
};
} // stomp
