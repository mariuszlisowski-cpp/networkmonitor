#pragma once

#include <nlohmann/json.hpp>

#include <filesystem>
#include <fstream>
#include <optional>
#include <string>

namespace nm {
class Downloader
{
public:
    bool downloadFile(const std::string& fileUrl,
                             const std::filesystem::path& destination,
                             const std::filesystem::path& caCertFile = {});
    std::optional<nlohmann::json> parseJsonFile(const std::filesystem::path& source);

private:
    std::ifstream cached_file;
};
} // namespace nm
