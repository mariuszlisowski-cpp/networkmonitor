#include <network-monitor/stomp-client.hpp>
#include <iostream>
#include <sstream>

namespace stomp {
StompClient::StompFrame StompClient::prepareLoginFrame(const std::string& command,
                                                       const std::string& version,
                                                       const std::string& host,
                                                       const std::string& login,
                                                       const std::string& password)
{
    return StompFrame{.command = command,
                      .headers = {{version_header_title, version},
                                  {host_header_title, host},
                                  {login_header_title, login},
                                  {password_header_title, password}},
                      .body = ""
                     };
}

/* unordered_map<>::at() std::out_of_range if the container does not have an element with the specified key */
std::string StompClient::convertFrameToString(const StompFrame& frame)
{
    std::stringstream ss;
    try {
        ss << frame.command << std::endl
           << version_header_title << frame.headers.at(version_header_title) << std::endl
           << host_header_title << frame.headers.at(host_header_title) << std::endl
           << login_header_title << frame.headers.at(login_header_title) << std::endl
           << password_header_title << frame.headers.at(password_header_title) << std::endl
           << std::endl
           << frame.body
           << '\0';
    }
    catch(const std::out_of_range& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }
    return ss.str();
}
} // stomp

/* 
STOMP \EOL
accept-version:1.2 \EOL
host:ltnm.learncppthroughprojects.com \EOL
login:<your_login> \EOL
passcode:<your_passcode> \EOL
\EOL
body
\NULL

The frame starts with a command string terminated by an end-of-line (EOL),
which consists of an OPTIONAL carriage return (octet 13) followed by a REQUIRED line feed (octet 10).

Following the command are zero or more header entries in <key>:<value> format. Each header entry is terminated by an EOL.
A blank line (i.e. an extra EOL) indicates the end of the headers and the beginning of the body.

The body is then followed by the NULL octet.
The examples in this document will use ^@, control-@ in ASCII, to represent the NULL octet.
The NULL octet can be optionally followed by multiple EOLs.
For more details, on how to parse STOMP frames, see the Augmented BNF section of this document.
*/
