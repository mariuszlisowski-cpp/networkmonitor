// #include "network-monitor/network-layout.hpp"
#include <stdexcept>
#include <memory>
#include <network-monitor/transport-network.hpp>

namespace nm {

bool TransportNetwork::addStation(const Station& station) {
    /* check if a station already exists in stations map*/
    if (getStation(station.id) != nullptr) {
        return false;
    }
    /* create a new node (station) */
    auto node = std::make_shared<Node>(Node{
        .stationId = station.id,
        .name = station.name,
        .passengerCount = 0,
        .edges = {}
    });
    /* insert into a stations map */
    stations.emplace(station.id, std::move(node));

    return true;
}

bool TransportNetwork::addLine(const Line& line) {
    /* check if a line already exists in lines map*/
    if (getLine(line.id) != nullptr) {
        return false;
    }
    /* create a new internal line */
    auto lineInternal = std::make_shared<LineInternal>(LineInternal{
        .id = line.id,
        .name = line.name,
        .routes = {}
    });
    /* add routes from line to the internal line */
    for (const Route& route : line.routes) {
        auto success = addRouteToLine(route, lineInternal);
        if (!success) {
            return false;
        }
    }

    lines.emplace(line.id, std::move(lineInternal));

    return true;
}

bool TransportNetwork::addRouteToLine(const Route& route, const std::shared_ptr<LineInternal>& lineInternal) {
    /* check if a route already exists in internal line */
    if (lineInternal->routes.find(route.id) != lineInternal->routes.end()) {
        return false;
    }
    /* add the list of stations (stops) to internal route - all stations must already be in the network */
    std::vector<std::shared_ptr<Node>> stops;
    stops.reserve(route.stops.size());                                          // same size as existing route stops
    for (const Id& stop : route.stops) {
        /* check if a station already exists in stations map*/
        const auto station = getStation(stop);
        if (station == nullptr) {
            return false;
        }
        stops.emplace_back(station);
    }
    /* create an internal route */
    auto routeInternal = std::make_shared<RouteInternal>(RouteInternal{
        .id = route.id,
        .line = lineInternal,
        .stops = std::move(stops)
    });
    /* walk the station nodes (stops) to add an edge for the route */
    for (size_t idx {0}; idx < routeInternal->stops.size() - 1; ++idx) {
        const std::shared_ptr<Node>& thisStop {routeInternal->stops[idx]};
        const std::shared_ptr<Node>& nextStop {routeInternal->stops[idx + 1]};
        thisStop->edges.emplace_back(std::make_shared<Edge>(Edge {
            .route = routeInternal,
            .nextStop = nextStop,
            .travelTime = 0,
        }));
    }
    /* add the internal route to the internal line */
    lineInternal->routes.emplace(route.id, std::move(routeInternal));

    return true;
}

std::shared_ptr<Node> TransportNetwork::getStation(const Id& id) const {
    return stations.count(id) > 0 ? stations.at(id) : nullptr;
}

std::shared_ptr<LineInternal> TransportNetwork::getLine(const Id& id) const {
    return lines.count(id) > 0 ? lines.at(id) : nullptr;
}

bool TransportNetwork::recordPassengerEvent(const PassengerEvent& event) {
    /* check if there is such a station at all */
    if (getStation(event.stationId) == nullptr) {
        return false;
    }

    auto& passengers = stations[event.stationId]->passengerCount;
    if (event.type == PassengerEvent::Type::IN) {
        ++passengers;
    }
    if (event.type == PassengerEvent::Type::OUT) {
        --passengers;
    }

    return true;
}

long long TransportNetwork::getPassengerCount(const Id& station) const {
    if (getStation(station) == nullptr) {
        throw std::runtime_error("No such station: " + station );
    }

    return  stations.at(station)->passengerCount;
}

std::vector<Id> TransportNetwork::getRoutesServingStation(const Id& station) const {
    /* check if there is such a station at all */
    if (getStation(station) == nullptr) {
        return {};
    }

    auto stationNode = stations.at(station);
    const auto& edges {stationNode->edges};
    std::vector<Id> routes;
    /* Iterate over all edges departing from the node. Each edge corresponds to one route serving the station. */
    for (const auto& edge : edges) {
        routes.push_back(edge->route->id);
    }
    /* The previous loop misses a corner case: The end station of a route does not have any edge containing that route,
       because we only track the routes that *leave from*, not *arrive to* a certain station.
       We need to loop over all line routes to check if our station is the end stop of any route.
       FIXME: In the worst case, we are iterating over all routes for all lines in the network.
              We may want to optimize this. */
    for (const auto& [_, line]: lines) {
        for (const auto& [_, route]: line->routes) {
            const auto& endStop {route->stops[route->stops.size() - 1]};
            if (stationNode == endStop) {
                routes.push_back(route->id);
            }
        }
    }

    return routes;
}

bool TransportNetwork::setTravelTime(const Id& stationA, const Id& stationB, const unsigned int travelTime) {


    return true;
}

unsigned int TransportNetwork::getTravelTime(const Id& stationA, const Id& stationB) const {


    return 0;
}

unsigned int TransportNetwork::getTravelTime(const Id& line,
                                             const Id& route,
                                             const Id& stationA,
                                             const Id& stationB) const {
    


    return 0;
}

} // namespace nm
