#include <network-monitor/file-downloader.hpp>

#include <cpr/api.h>
#include <cpr/cprtypes.h>
#include "cpr/error.h"
#include <cpr/response.h>

#include <filesystem>
#include <iostream>

namespace nm {
bool Downloader::downloadFile(const std::string& fileUrl,
                              const std::filesystem::path& destination,
                              const std::filesystem::path& caCertFile)
{
    cpr::Response response = cpr::Get(cpr::Url{fileUrl});
    if (response.error.code != cpr::ErrorCode::OK)
    {
        std::cerr << "ERR: download error! Error code: "
                  << static_cast<std::underlying_type<cpr::ErrorCode>::type>(response.error.code) << std::endl;
        return false;
    }
    std::cout << "INF: download success! Status code: " << response.status_code << std::endl;

    std::ofstream ofs(destination);
    ofs << response.text;
    ofs.close();

    return true;
}

std::optional<nlohmann::json> Downloader::parseJsonFile(const std::filesystem::path& source)
{
    if (!std::filesystem::exists(source))
    {
        std::cerr << "ERR: " << __func__ << ": file does not exist in the path!" << std::endl;
        return std::nullopt;
    }

    cached_file = std::ifstream(source);
    nlohmann::json parsed;
    try {
        cached_file >> parsed;
        // std::ifstream file(source);
        // file >> parsed;
    } catch (const std::exception& e) {
        std::cerr << "ERR: cannot parse input file! Exception: " << e.what() << std::endl;
    }

    return parsed;
}
} // namespace nm
