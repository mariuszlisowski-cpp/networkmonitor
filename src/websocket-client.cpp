#include <boost/asio/ssl/stream_base.hpp>
#include <boost/beast/core/stream_traits.hpp>
#include <network-monitor/websocket-client.hpp>

#include <boost/asio/strand.hpp>
#include <boost/beast/core/buffers_to_string.hpp>
#include <openssl/ssl.h>

namespace nm {
static void log(const std::string& error, boost::system::error_code ec)
{
    std::cerr << "[" << error << "] "
              << (ec ? "output: " : "OK")
              << (ec ? ec.message() : "")
              << std::endl;
}

WebSocketClient::WebSocketClient(const std::string& url,
                                 const std::string& endpoint,
                                 const std::string& port,
                                 boost::asio::io_context& io_context,
                                 boost::asio::ssl::context& tls_context)
    : url(url)
    , endpoint(endpoint)
    , port(port)
    , resolver(boost::asio::make_strand(io_context))
    , websocket(boost::asio::make_strand(io_context), tls_context) {}

WebSocketClient::~WebSocketClient() = default;

/**** public methods ****/

void WebSocketClient::connect(
        std::function<void(const boost::system::error_code&)> onConnect_user,
        std::function<void(const boost::system::error_code&, std::string&& message)> onReceive_user,
        std::function<void(const boost::system::error_code&)> onDisconnect_user)
{
    /* save user callbacks for later using private functon object membmers */
    onConnect_ = onConnect_user;
    onReceive_ = onReceive_user;
    onDisconnect_ = onDisconnect_user;

    closed = false;
    resolver.async_resolve(url, port,
                           [this](auto ec, auto result) {
                               onResolve(ec, result);
                           });
}

void WebSocketClient::send(
        const std::string& message,
        std::function<void(const boost::system::error_code& ec)> onSend_user)
{
    websocket.async_write(boost::asio::buffer(message),
                          [onSend_user](auto ec, auto bytes_transferred) {
                              if (onSend_user) {
                                  onSend_user(ec);
                              }
                          });
}

void WebSocketClient::close(std::function<void(const boost::system::error_code& ec)> onClose_user)
{
    closed = true;
    websocket.async_close(boost::beast::websocket::close_code::none,
                         [onClose_user](auto ec) {
                             if (onClose_user) {
                                 log("Disconnected", ec);
                                 std::cerr << std::endl;
                                 onClose_user(ec);
                             }
                         });
}

/**** private methods ****/

void WebSocketClient::onResolve(
    const boost::system::error_code& ec,
    const boost::asio::ip::tcp::resolver::results_type& endpoint)
{
    if (ec)
    {
        log("Hostname resolving error", ec);
        // if (onConnect_) {
        //     onConnect_(ec);
        // }
        return;
    }
    log("Hostname resolving success", ec);
    
    /* we will reset the timeout to a sensible default after we are connected
    websocket.next_layer().next_layer().expires_after(std::chrono::seconds(5)); */
    boost::beast::get_lowest_layer(websocket).expires_after(std::chrono::seconds(5));

    websocket.next_layer().next_layer().async_connect(*endpoint,
                                                      [this](auto ec) {
                                                          onConnect(ec);
                                                      });
}

void WebSocketClient::onConnect(const boost::system::error_code& ec)
{
    if (ec)
    {
        log("TCP socket connection failed", ec);
        // if (onConnect_) {
        //     onConnect_(ec);
        // }
        return;
    }
    log("TCP socket connection success", ec);

    /* now that the TCP socket is connected,
       we can reset the timeout to whatever Boost.Beast recommends */
    websocket.next_layer().next_layer().expires_never();
    websocket.set_option(
        boost::beast::websocket::stream_base::timeout::suggested(boost::beast::role_type::client));

    /* some clients require that we set the host name before the TLS handshake
       or the connection will fail - we use an OpenSSL function for that */
    SSL_set_tlsext_host_name(websocket.next_layer().native_handle(), url.c_str());

    websocket.next_layer().async_handshake(boost::asio::ssl::stream_base::client,
                                           [this](auto ec){
                                               onTlsHandshake(ec);
                                           });
}

void WebSocketClient::onTlsHandshake(const boost::system::error_code& ec)
{
    if (ec)
    {
        log("TLS handshake failed", ec);
        return;
    }
    log("TLS handshake success", ec);

    websocket.async_handshake(url,
                              endpoint,
                              [this](auto ec) {
                                  onHandshake(ec);
                              });
}

void WebSocketClient::onHandshake(const boost::system::error_code& ec)
{
    if (ec)
    {
        log("Websocket handshake failed", ec);
        // if (onConnect_) {
        //     onConnect_(ec);
        // }
        return;
    }
    log("Websocket handshake success", ec);

    websocket.text(true);

    /* set up a recursive asynchronous listener to receive messages */
    listenToIncomingMessage(ec);

    /* dispatch the user callback - 
       this call is synchronous and will block the WebSocket strand */
    if (onConnect_) {
        onConnect_(ec);
    }
}

void WebSocketClient::listenToIncomingMessage(const boost::system::error_code& ec)
{
    if (ec == boost::asio::error::operation_aborted) {
        /* we check the 'closed' flag to avoid notifying the user twice */
        // if (onDisconnect_ && ! closed) {
        //     onDisconnect_(ec);
        // }
        return;
    }

    /* on a successful read, process the message
       and recursively call this function again to process the next message */
    websocket.async_read(read_buffer,
                         [this](auto ec, auto bytes_transferred) {
                             onRead(ec, bytes_transferred);
                             listenToIncomingMessage(ec);
                         });
}

void WebSocketClient::onRead(const boost::system::error_code& ec, size_t bytes_transferred)
{
    /* ignore read error */
    if (ec) {
        return;
    }
    log("Message received", ec);

    /* Parse the message and forward it to the user callback -
       this call is synchronous and will block the WebSocket strand */
    std::string message {boost::beast::buffers_to_string(read_buffer.data())};
    read_buffer.consume(bytes_transferred);
    if (onReceive_) {
        onReceive_(ec, std::move(message));
    }
}
} // namespace nm
