cmake_minimum_required(VERSION 3.17 FATAL_ERROR)
project(network-monitor)

set(CMAKE_BUILD_TYPE Release)

if(EXISTS ${CMAKE_CURRENT_BINARY_DIR}/conaninfo.txt)
    list(PREPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_BINARY_DIR}")
endif()

find_package(Boost 1.74 REQUIRED COMPONENTS system)
find_package(OpenSSL 3.0.1 REQUIRED)
find_package(cpr REQUIRED)
find_package(nlohmann_json 3.10.5 REQUIRED)

# library
set(SOURCES
    src/websocket-client.cpp
    src/stomp-client.cpp
    src/file-downloader.cpp
    src/transport-network.cpp
)
add_library(${PROJECT_NAME}-lib STATIC ${SOURCES})
target_include_directories(${PROJECT_NAME}-lib PUBLIC inc)
target_compile_features(${PROJECT_NAME}-lib PUBLIC cxx_std_17)
target_link_libraries(${PROJECT_NAME}-lib
    PUBLIC 
        Boost::Boost
        OpenSSL::OpenSSL
        nlohmann_json::nlohmann_json
    PRIVATE
        cpr::cpr
)

# tests
set(TESTS
    test/main-ut.cpp
    # test/websocket-client-ut.cpp
    # test/file-downloader-ut.cpp
    test/transport-network-ut.cpp
)
add_executable(${PROJECT_NAME}-ut ${TESTS})
target_include_directories(${PROJECT_NAME}-ut PRIVATE inc)
target_compile_features(${PROJECT_NAME}-ut PUBLIC cxx_std_17)
target_compile_definitions(${PROJECT_NAME}-ut
    PRIVATE
        TEST_CACERT_PEM="${CMAKE_CURRENT_SOURCE_DIR}/test/cacert.pem"
        TEST_NETWORK_LAYOUT="${CMAKE_CURRENT_SOURCE_DIR}/test/network-layout.json"
)
target_link_libraries(${PROJECT_NAME}-ut
    PRIVATE
        ${PROJECT_NAME}-lib
        nlohmann_json::nlohmann_json
)

enable_testing()
add_test(NAME network-monitor-test COMMAND ${PROJECT_NAME}-ut)
