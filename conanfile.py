from conans import ConanFile

class ConanPackage(ConanFile):
    name = 'network-monitor'
    version = "0.1.0"

    generators = 'cmake_find_package'

    requires = [
        ('boost/1.74.0'),
        ('openssl/3.0.1'),
        ('cpr/1.7.2'),
        ('nlohmann_json/3.10.5')
    ]

    default_options = (
        'boost:shared=False',
    )
